use std::io::{self, Read};

#[cfg(test)]
use crate::types::{
    Certificate, CertificateData, Header, Signature, SignatureAlgorithm, SignatureParams,
};

mod certs;
mod types;

fn main() {
    let mut cert = String::new();
    let mut input = String::new();
    loop {
        io::stdin().read_to_string(&mut input).unwrap();

        if input == "" || input == "\n" {
            break;
        }
        cert.extend(input.chars());
        input.clear();
    }

    let cert = certs::CertParser::new()
        .parse(&cert)
        .expect("Error parsing certificate");

    let serialized = serde_json::to_string(&cert).expect("Error serializing certificate");
    println!("{}", serialized);
}

#[test]
fn test_basic() {
    let cert_str = include_str!("../examples/home-agh-edu-pl.deser");
    let cert = certs::CertParser::new().parse(cert_str).unwrap();

    let target = Certificate {
        header: Header {
            version: 2,
            length: 8,
        },
        data: CertificateData {
            serial_number: "309226622480169992721990743296804148600".into(),
            signature: Signature {
                algorithm: SignatureAlgorithm::SHA384WithRSAEncryption,
                params: vec![SignatureParams::AnyNull],
            },
            issuer: vec![
                ("country".into(), "NL".into()),
                ("organization".into(), "GEANT Vereniging".into()),
                ("name".into(), "GEANT OV RSA CA 4".into()),
            ],
            validity: ("2023-06-26 00:00:00".into(), "2024-06-25 23:59:59".into()),
            subject: vec![
                ("country".into(), "PL".into()),
                ("state".into(), "Małopolskie".into()),
                (
                    "organization".into(),
                    "Akademia Gorniczo Hutnicza im Stanisława Staszica w Krakowie".into(),
                ),
                ("name".into(), "home.agh.edu.pl".into()),
            ],
            subject_key: "309480713252648985754244152355695738792452759989343480480998029346644…"
                .into(),
        },
        sig: "000111000001000111000010001100011111011001110001111010111000000011001…".into(),
    };

    assert_eq!(cert, target)
}
