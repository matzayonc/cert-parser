use serde::Serialize;

#[derive(PartialEq, Debug, Serialize)]
pub struct Certificate {
    pub header: Header,
    pub data: CertificateData,
    pub sig: String,
}

#[derive(PartialEq, Debug, Serialize)]
pub struct Header {
    pub version: u8,
    pub length: u8,
}

#[derive(PartialEq, Debug, Serialize)]
pub struct CertificateData {
    pub serial_number: String,
    pub signature: Signature,
    pub issuer: Vec<(String, String)>,
    pub validity: (String, String),
    pub subject: Vec<(String, String)>,
    pub subject_key: String,
}

#[derive(PartialEq, Debug, Serialize)]
pub struct Signature {
    pub algorithm: SignatureAlgorithm,
    pub params: Vec<SignatureParams>,
    // pub value: Vec<u8>,
}

#[derive(PartialEq, Debug, Serialize)]
pub enum SignatureAlgorithm {
    SHA384WithRSAEncryption,
}

#[derive(PartialEq, Debug, Serialize)]
pub enum SignatureParams {
    AnyNull,
}
