fn main() {
    lalrpop::Configuration::new()
        .process_file("src/certs.lalrpop")
        .expect("Failed to process lalrpop grammar");
}
