# Parser of 802.1X certificates

Implemented as a practical application of a parser for "Methods and algorithm of compilation" at AGH University of Science and Technology.

## Input language

Program loads a file with data needed in a 802.1X certificate and outputs in DER form.
